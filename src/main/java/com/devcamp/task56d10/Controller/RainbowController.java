package com.devcamp.task56d10.Controller;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.task56d10.Service.RainbowService;

@RestController
@CrossOrigin
public class RainbowController {
    @Autowired
    RainbowService RainbowService;
    @GetMapping("/rainbow-request-query")
    public ArrayList <String> getRequestQuery(@RequestParam(name = "id", defaultValue = "" ) String key){
        return RainbowService.filter(key);
    }

    @GetMapping("/rainbow-request-param/{index}")
    public String getStrings(@PathVariable int index){
        return RainbowService.filterString(index);
    }
}
