package com.devcamp.task56d10.Service;

import java.util.ArrayList;

import org.springframework.stereotype.Service;

@Service
public class RainbowService {
    String[] listStrings = { "red", "orange", "yellow", "green", "blue", "indigo", "violet" };

    public ArrayList<String> filter(String key) {
        ArrayList<String> fillter = new ArrayList<>();
        for (String element : listStrings) {
            if (element.contains(key)) {
                fillter.add(element);
            }
        }

        return fillter;
    }

    public String filterString(int index) {
        String string = "";

        if (index > -1 && index <= 6) {
            string = listStrings[index];
        }

        return string;
    }
}
