package com.devcamp.task56d10;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Task56D10Application {

	public static void main(String[] args) {
		SpringApplication.run(Task56D10Application.class, args);
	}

}
